import React from 'react';
import renderer from 'react-test-renderer';
import Enzyme, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { MemoryRouter as Router, Link } from 'react-router-dom';

Enzyme.configure({ adapter: new Adapter() });

import DeleteButton from '../../../src/js/common/components/TaskList/DeleteButton';

describe('DeleteButton', () => {
  it('should render a correct structure with value', () => {
    const wrapper = renderer
      .create(
        <Router>
          <DeleteButton link="/delete/25" />
        </Router>
      )
      .toJSON();
    expect(wrapper).toMatchSnapshot();
  });

  it('should show modal after click', () => {
    const wrapper = mount(
      <Router>
        <DeleteButton link="/delete/25" />
      </Router>
    );

    const event = { preventDefault: jest.fn() };
    const button = wrapper.find(DeleteButton).instance();

    const spy = jest.spyOn(button, 'handleClick');

    button.handleClick(event);
    console.log('link.instance', button);

    expect(event.preventDefault.mock.calls.length).toBe(1);
    expect(spy).toHaveBeenCalled();
    expect(button.state.isShowingModal).toBe(true);

    spy.mockReset();
    spy.mockRestore();
  });

  it('should hide modal after close', () => {
    const wrapper = mount(
      <Router>
        <DeleteButton link="/delete/25" />
      </Router>
    );

    const button = wrapper.find(DeleteButton).instance();

    const spy = jest.spyOn(button, 'handleClose');

    button.handleClose();

    expect(button.state.isShowingModal).toBe(false);
    expect(spy).toHaveBeenCalled();

    spy.mockReset();
    spy.mockRestore();
  });
});
