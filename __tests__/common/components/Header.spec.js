import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

import { Header } from '../../../src/js/common/components/Header';

const fixture = {
  location: { pathname: '' },
};

describe('HeaderView', () => {
  it('should render a correct structure', () => {
    const el = shallow(<Header {...fixture} />);

    expect(el.length).toEqual(1);
    expect(el.find('.logo').length).toEqual(1);
    expect(el.find('li').length).toEqual(3);
    expect(el.find('.active').length).toEqual(1);
  });
});
