import React from 'react';
import renderer from 'react-test-renderer';
import Enzyme, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

import InputField from '../../../src/js/common/components/Task/InputField';

describe('InputField', () => {
  it('should render a correct structure with value', () => {
    const input = renderer
      .create(<InputField hint="Simple Hint" title="Enter simple text" value="Hello" />)
      .toJSON();
    expect(input).toMatchSnapshot();
  });

  it('should render a correct structure if blank', () => {
    const input = renderer
      .create(<InputField hint="Simple Hint" title="Enter simple text" />)
      .toJSON();
    expect(input).toMatchSnapshot();
  });

  it('should call handleFocus', async () => {
    const mockOnChange = jest.fn();
    const wrapper = mount(
      <InputField
        hint="Simple Hint"
        title="Enter simple text"
        value="Hello"
        onChange={mockOnChange}
      />
    );

    const mockInputFocus = jest.fn();
    const input = wrapper.find('input').instance();

    input.focus = mockInputFocus;
    expect(mockInputFocus.mock.calls.length).toBe(0);
    wrapper.find('.task__field').simulate('click');
    expect(mockInputFocus.mock.calls.length).toBe(1);
  });

  it('should call onChange', async () => {
    const mockOnChange = jest.fn();
    const wrapper = mount(
      <InputField
        hint="Simple Hint"
        title="Enter simple text"
        value="Hello"
        onChange={mockOnChange}
      />
    );

    expect(mockOnChange.mock.calls.length).toBe(0);
    wrapper.find('input').simulate('change');
    expect(mockOnChange.mock.calls.length).toBe(1);
  });
});
