import React from 'react';
import renderer from 'react-test-renderer';
import Enzyme, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

import ServiceTypeItem from '../../../src/js/common/components/Task/ServiceTypeItem';

const fixture = {
  id: 777,
  title: 'Electrician',
  icon: '/image/blank.png',
};

describe('ServiceTypeItem', () => {
  it('should render a correct structure with selected true', () => {
    const input = renderer
      .create(<ServiceTypeItem {...fixture} selected onPress={jest.fn()} />)
      .toJSON();
    expect(input).toMatchSnapshot();
  });

  it('should render a correct structure with selected false', () => {
    const input = renderer
      .create(<ServiceTypeItem {...fixture} selected={false} onPress={jest.fn()} />)
      .toJSON();
    expect(input).toMatchSnapshot();
  });

  it('should call onPress', async () => {
    const mockOnPress = jest.fn();
    const wrapper = mount(<ServiceTypeItem {...fixture} selected={false} onPress={mockOnPress} />);

    expect(mockOnPress.mock.calls.length).toBe(0);
    wrapper.find('.task__serviceType__item').simulate('click');
    expect(mockOnPress.mock.calls.length).toBe(1);
    expect(mockOnPress.mock.calls[0][0]).toBe(fixture.id);
  });
});
