import ServiceTypes from '../../../src/js/common/components/Task/ServiceTypes';

describe('ServiceTypes Structure is correct', () => {
  it('should have a correct structure', () => {
    expect(ServiceTypes).toMatchSnapshot();
  });
});
