import React from 'react';
import renderer from 'react-test-renderer';
import Enzyme, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

import Task from '../../../src/js/common/components/Task';
import { prepareDescription, EditTask } from '../../../src/js/common/components/Task/Task';
import TaskItem from '../../../src/js/common/components/Task/TaskItem';

const fixture = {
  previewTitle: 'New Task',
  actionUrl: '/create',
  actionTitle: 'Create Task',
  mapLastPosition: null,
  task: null,
  onTask: jest.fn(),
};

const taskFixture = {
  serviceType: 0,
  tasks: [0, 1],
  taskDescription: 'Please!! help',
  location: 'My City',
  position: { lat: 0.5, lng: 0.25 },
  description: '',
};

const mapLastPositionFixture = { address: 'My New City', latLng: { lat: 0.75, lng: 0.15 } };

describe('TaskView', () => {
  it('should render a correct structure with value', () => {
    let wrapper = renderer.create(<Task {...fixture} />).toJSON();
    expect(wrapper).toMatchSnapshot();

    wrapper = renderer.create(<Task {...fixture} task={taskFixture} />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });

  it('should setState after change prop task', () => {
    const wrapper = mount(<Task onTask={jest.fn()} />);

    expect(wrapper.find('.task__preview__description').text()).toBe('I need...');

    wrapper.setProps({ task: taskFixture });
    expect(wrapper.state()).toMatchSnapshot();
  });

  it('should call setPreviewDescription after change prop mapLastPosition', () => {
    const wrapper = mount(<Task onTask={jest.fn()} />);

    wrapper.setProps({ task: taskFixture });

    const task = wrapper.instance();
    const spy = jest.spyOn(task, 'setPreviewDescription');

    wrapper.setProps({ mapLastPosition: mapLastPositionFixture });

    expect(spy).toHaveBeenCalled();

    spy.mockReset();
    spy.mockRestore();
  });

  it('should call handleCompleteEdit & onTask', () => {
    const mockOnTask = jest.fn();
    const wrapper = mount(<Task onTask={mockOnTask} task={taskFixture} />);

    expect(mockOnTask.mock.calls.length).toBe(0);

    wrapper
      .find('.task__preview__createtask')
      .find('a')
      .simulate('click');

    expect(mockOnTask.mock.calls.length).toBe(1);
    expect(mockOnTask.mock.calls[0][0]).toMatchSnapshot();
  });

  it('should call click -> handleTaskItemSelect -> setPreviewDescription', () => {
    const mockOnTask = jest.fn();
    const wrapper = mount(<Task onTask={mockOnTask} task={taskFixture} />);

    const task = wrapper.instance();
    const spy = jest.spyOn(task, 'setPreviewDescription');

    const oldState = wrapper.state();
    expect(wrapper.find('.task__taskTag__item').length).toBe(6);
    wrapper
      .find('.task__taskTag__item')
      .at(5)
      .simulate('click');
    expect(wrapper.state()).not.toEqual(oldState);

    expect(spy).toHaveBeenCalled();

    spy.mockReset();
    spy.mockRestore();
  });

  it('should call click -> handleServiceTypeSelect -> setPreviewDescription', () => {
    const mockOnTask = jest.fn();
    const wrapper = mount(<Task onTask={mockOnTask} task={taskFixture} />);

    const task = wrapper.instance();
    const spy = jest.spyOn(task, 'setPreviewDescription');

    const oldState = wrapper.state();
    expect(wrapper.find('.task__serviceType__item').length).toBe(5);
    wrapper
      .find('.task__serviceType__item')
      .at(4)
      .simulate('click');
    expect(wrapper.state()).not.toEqual(oldState);

    expect(spy).toHaveBeenCalled();

    spy.mockReset();
    spy.mockRestore();
  });

  it('should call handleChangeDescription -> setPreviewDescription', () => {
    const mockOnTask = jest.fn();
    const wrapper = mount(<Task onTask={mockOnTask} task={taskFixture} />);

    const task = wrapper.instance();
    const spy = jest.spyOn(task, 'setPreviewDescription');

    const oldState = wrapper.state();
    task.handleChangeDescription({ target: { value: 'new description' } });
    expect(wrapper.state()).not.toEqual(oldState);
    expect(spy).toHaveBeenCalled();

    spy.mockReset();
    spy.mockRestore();
  });

  it("should prepareDescription return '' ", () => {
    expect(prepareDescription('')).toBe('');
  });

  // EditTask
  it('EditTask should have correct props ', () => {
    const mockOnTask = jest.fn();
    const wrapper = mount(<EditTask task={taskFixture} />);

    expect(wrapper.props()).toMatchSnapshot();
  });
});
