import React from 'react';
import renderer from 'react-test-renderer';
import Enzyme, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { MemoryRouter as Router, withRouter } from 'react-router-dom'; // 4.0.0

Enzyme.configure({ adapter: new Adapter() });

import TaskList from '../../../src/js/common/components/TaskList';

const fixture = [
  {
    id: '777',
    created: new Date(2008, 0, 7, 6, 30),
    description: 'Simple task',
  },
];

describe('TaskListView', () => {
  it('should render a correct structure with value', () => {
    const wrapper = mount(
      <Router>
        <TaskList tasks={fixture} />
      </Router>
    );
    expect(wrapper.html()).toMatchSnapshot();
  });
});
