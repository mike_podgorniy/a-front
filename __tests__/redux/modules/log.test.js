import nanoid from 'nanoid';
import { taskTypes, taskActions } from '../../../src/js/redux/modules/task';
import reducers, {
  HANDLERS,
  INITIAL_STATE,
  logActions,
  logTypes,
} from '../../../src/js/redux/modules/log';

import { getStore } from '../../../__fixtures__/store';

const getFixture = type => ({
  id: '777',
  task: { description: 'Simple description', location: 'My City', id: '777' },
  type,
});

const getTaskFixture = (type) => {
  const time = Date.now();
  const id = nanoid();
  const description = 'Simple description';
  const location = 'Simple description';
  return {
    id,
    task: {
      id,
      description,
      location,
      updated: time,
      created: time,
      type,
    },
    log: {
      action: {
        id,
        description: type === taskTypes.DELETE_SUCCESS ? undefined : description,
        location: type === taskTypes.DELETE_SUCCESS ? undefined : location,
        type,
      },
      at: time,
    },
  };
};

describe('redux.modules.log', () => {
  let store = null;

  beforeEach(() => {
    store = getStore({
      log: INITIAL_STATE,
    });
  });

  afterEach(() => {
    store = null;
  });

  it('should return correct state when log CREATE_SUCCESS', () => {
    const type = taskTypes.CREATE_SUCCESS;
    const state = store.getState();
    const log = state.log;
    const fixture = getTaskFixture(type);

    const result = reducers(log, taskActions.createSuccess(fixture.task));

    expect(result.asMutable()[0].action).toEqual(fixture.log.action);
  });

  it('should return correct state when log DELETE_SUCCESS', () => {
    const type = taskTypes.DELETE_SUCCESS;
    const state = store.getState();
    const log = state.log;
    const fixture = getTaskFixture(type);

    const result = reducers(log, taskActions.deleteSuccess(fixture.id));

    expect(result.asMutable()[0].action).toEqual(fixture.log.action);
  });

  it('should return correct state when log RESET', () => {
    const type = taskTypes.DELETE_SUCCESS;
    const state = store.getState();
    const log = state.log;
    const fixture = getTaskFixture(type);

    const result = reducers(log, taskActions.deleteSuccess(fixture.id));
    const result2 = reducers(result, logActions.reset());

    expect(result2.asMutable()).toEqual([]);
  });
});
