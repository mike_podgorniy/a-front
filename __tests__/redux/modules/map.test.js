import nanoid from 'nanoid';
import { taskTypes, taskActions } from '../../../src/js/redux/modules/task';
import reducers, {
  HANDLERS,
  INITIAL_STATE,
  mapActions,
  mapTypes,
} from '../../../src/js/redux/modules/map';

import { getStore } from '../../../__fixtures__/store';

const fixture = {};

describe('redux.modules.map', () => {
  let store = null;

  beforeEach(() => {
    store = getStore({
      log: INITIAL_STATE,
    });
  });

  afterEach(() => {
    store = null;
  });

  it('should return correct state when CHANGE_LAST_POSITION', () => {
    const state = store.getState();
    const map = state.map;

    const result = reducers(map, mapActions.changeLastPosition(fixture));

    expect(result.asMutable().lastPosition).toEqual(fixture);
  });

  it('should return correct state when CREATE_SUCCESS', () => {
    const state = store.getState();
    const map = state.map;

    const result = reducers(map, mapActions.changeLastPosition(fixture));
    const result2 = reducers(map, taskActions.createSuccess({}));

    expect(result2.asMutable().lastPosition).toEqual(null);
  });
});
