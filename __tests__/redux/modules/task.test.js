import reducers, {
  HANDLERS,
  INITIAL_STATE,
  taskActions,
  taskTypes,
} from '../../../src/js/redux/modules/task';

import { getStore } from '../../../__fixtures__/store';

const getFixture = type => ({
  id: '777',
  task: { description: 'Simple description', location: 'My City', id: '777' },
  type,
});

const getTaskFixture = () => {
  const time = Date.now();
  const description = 'Simple description';
  const location = 'Simple description';
  return {
    id: 777,
    task: {
      id: 777,
      description,
      location,
      updated: time,
      created: time,
    },
  };
};

describe('redux.modules.task', () => {
  let store = null;

  beforeEach(() => {
    store = getStore({
      task: INITIAL_STATE,
    });
  });

  afterEach(() => {
    store = null;
  });

  it('should return correct state when task CREATE_SUCCESS', () => {
    const state = store.getState();
    const task = state.task;
    const fixture = getTaskFixture();

    const result = reducers(task, taskActions.createSuccess(fixture.task));

    expect(result.asMutable().data[0]).toEqual(fixture.task);
  });

  it('should return correct state when task EDIT_SUCCESS', () => {
    const state = store.getState();
    const task = state.task;
    const fixture = getTaskFixture();

    const result = reducers(task, taskActions.createSuccess(fixture.task));
    const result2 = reducers(
      result,
      taskActions.editSuccess(fixture.id, { ...fixture.task, describe: 'new' })
    );

    expect(result2.asMutable().data[0]).toEqual({ ...fixture.task, describe: 'new' });
  });

  it('should return correct state when task DELETE_SUCCESS', () => {
    const state = store.getState();
    const task = state.task;
    const fixture = getTaskFixture();

    console.log();
    const result = reducers(task, taskActions.createSuccess(fixture.task));
    const result2 = reducers(result, taskActions.deleteSuccess(fixture.id));

    expect(result2.asMutable().data).toEqual([]);
  });

  it('should return correct state when task UPDATE_SUCCESS', () => {
    const state = store.getState();
    const task = state.task;
    const fixture = getTaskFixture();

    const result = reducers(task, taskActions.updateSuccess([fixture.task]));

    expect(result.asMutable().data[0]).toEqual(fixture.task);
  });
});
