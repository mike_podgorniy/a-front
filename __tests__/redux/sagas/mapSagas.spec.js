import { put, call, takeLatest, all } from 'redux-saga/effects';
import rootSaga, { changeLastPosition, requestWrapper } from '../../../src/js/redux/sagas/mapSaga';
import { mapActions, mapTypes } from '../../../src/js/redux/modules/map';

describe('redux.sagas.mapSaga', () => {
  describe('root Saga', () => {
    it('return root saga', () => {
      /*
      export default function* root() {
        yield all([yield takeLatest(types.CLICK, changeLastPosition)]);
      } */
      const generator = rootSaga();
      const takeMapClick = takeLatest(mapTypes.CLICK, changeLastPosition);

      expect(generator.next().value).toEqual(takeMapClick);
      expect(generator.next(takeMapClick).value).toEqual(all([takeMapClick]));
    });
  });

  const addresses = addresses =>
    addresses.map(address => ({
      short_name: address,
    }));

  const geocode = (_, func) => {
    const results = [{ address_components: addresses(['a', 'b', 'c', 'd']) }];
    const status = 'OK';
    func(results, status);
  };

  const fixture = {
    geocoder: {
      geocode,
    },
    latLng: { lat: 0.5, lng: 0.25 },
    address: 'a, b, c, d',
  };

  describe('changeLastPosition', () => {
    it('should put correct action with correct data', () => {
      const action = mapActions.click(fixture.geocoder, fixture.latLng);
      const generator = changeLastPosition(action);

      const next = generator.next();

      expect(next.value).toEqual(call(requestWrapper, geocode, { location: fixture.latLng }));

      const next2 = generator.next(fixture.address);
      expect(next2.value).toEqual(
        put(mapActions.changeLastPosition({ address: fixture.address, latLng: fixture.latLng }))
      );
    });

    it('should put correct action with wrong data', () => {
      const generator = changeLastPosition();

      const next = generator.next();
      expect(next.value).toEqual(
        put(mapActions.error(new TypeError("Cannot read property 'geocoder' of undefined")))
      );
    });
  });

  describe('requestWrapper', () => {
    it('should resolve address', () => {
      const result = requestWrapper(geocode, { location: fixture.latLng });
      expect(result).resolves.toEqual(fixture.address);
    });

    it("should reject 'Cannot read property '0' of undefined'", () => {
      const result = requestWrapper(
        (_, func) => {
          const results = undefined;
          const status = 'OK';
          func(results, status);
        },
        { location: fixture.latLng }
      );
      // [TypeError: Cannot read property '0' of undefined]
      expect(result).rejects.toEqual(new TypeError("Cannot read property '0' of undefined"));
    });

    it("should reject 'No results found'", () => {
      const result = requestWrapper(
        (_, func) => {
          const results = [];
          const status = 'OK';
          func(results, status);
        },
        { location: fixture.latLng }
      );
      expect(result).rejects.toEqual('No results found');
    });

    it("should reject 'Geocoder failed due to: FAIL'", () => {
      const result = requestWrapper(
        (_, func) => {
          const results = undefined;
          const status = 'FAIL';
          func(results, status);
        },
        { location: fixture.latLng }
      );
      expect(result).rejects.toEqual('Geocoder failed due to: FAIL');
    });

    it("should reject 'not a function'", () => {
      const result = requestWrapper(null, { location: fixture.latLng });
      expect(result).rejects.toEqual(new Error('not a function'));
    });
  });
});
