import { all, fork } from 'redux-saga/effects';
import rootSaga from '../../../src/js/redux/sagas';
import taskSaga from '../../../src/js/redux/sagas/taskSaga';
import mapSaga from '../../../src/js/redux/sagas/mapSaga';

describe('redux.sagas.rootSaga', () => {
  describe('root Saga', () => {
    it('return root saga', () => {
      const generator = rootSaga();
      expect(generator.next().value).toEqual(all([fork(taskSaga), fork(mapSaga)]));
    });
  });
});
