import { put, call, takeLatest, all } from 'redux-saga/effects';
import { REHYDRATE } from 'redux-persist/constants';
import rootSaga, {
  createTask,
  editTask,
  deleteTask,
  updateTask,
  apiInstance,
  getBaseURL,
} from '../../../src/js/redux/sagas/taskSaga';
import { taskActions, taskTypes } from '../../../src/js/redux/modules/task';

describe('redux.sagas.taskSaga', () => {
  describe('root Saga', () => {
    it('return root saga', () => {
      const generator = rootSaga();
      const sagas = {
        takeCreateRequest: takeLatest(taskTypes.CREATE_REQUEST, createTask, apiInstance),
        takeEditRequest: takeLatest(taskTypes.EDIT_REQUEST, editTask, apiInstance),
        takeDeleteRequest: takeLatest(taskTypes.DELETE_REQUEST, deleteTask, apiInstance),
        takeUpdateRequest: takeLatest(
          [taskTypes.UPDATE_REQUEST, REHYDRATE],
          updateTask,
          apiInstance
        ),
      };

      expect(generator.next().value).toEqual(sagas.takeCreateRequest);
      expect(generator.next(sagas.takeCreateRequest).value).toEqual(all([sagas.takeCreateRequest]));

      expect(generator.next().value).toEqual(sagas.takeEditRequest);
      expect(generator.next(sagas.takeEditRequest).value).toEqual(all([sagas.takeEditRequest]));

      expect(generator.next().value).toEqual(sagas.takeDeleteRequest);
      expect(generator.next(sagas.takeDeleteRequest).value).toEqual(all([sagas.takeDeleteRequest]));

      expect(generator.next().value).toEqual(sagas.takeUpdateRequest);
      expect(generator.next(sagas.takeUpdateRequest).value).toEqual(all([sagas.takeUpdateRequest]));
    });
  });

  const fixture = {
    id: 777,
    task: {
      description: 'Doit!',
    },
  };

  describe('createTask', () => {
    it('should put correct action with correct data', () => {
      const action = taskActions.createRequest(fixture.task);
      const generator = createTask(apiInstance, action);

      expect(generator.next().value).toEqual(call(apiInstance.createTask, fixture.task));
      expect(generator.next(fixture.task).value).toEqual(
        put(taskActions.createSuccess(fixture.task))
      );
      expect(generator.next().value).toEqual(put(taskActions.updateRequest()));
    });

    it('should put correct action with wrong data', () => {
      const action = taskActions.createRequest(fixture.task);
      const generator = createTask(apiInstance);

      expect(generator.next().value).toEqual(
        put(taskActions.createFailure(new TypeError("Cannot read property 'task' of undefined")))
      );
    });
  });

  describe('editTask', () => {
    it('should put correct action with correct data', () => {
      const action = taskActions.editRequest(fixture.id, fixture.task);
      const generator = editTask(apiInstance, action);

      expect(generator.next().value).toEqual(call(apiInstance.editTask, fixture.id, fixture.task));

      expect(generator.next(fixture.task).value).toEqual(
        put(taskActions.editSuccess(fixture.id, fixture.task))
      );
    });

    it('should put correct action with wrong data', () => {
      const action = taskActions.editRequest(fixture.id, fixture.task);
      const generator = editTask(apiInstance);

      expect(generator.next().value).toEqual(
        put(taskActions.editFailure(new TypeError("Cannot read property 'task' of undefined")))
      );
    });
  });

  describe('deleteTask', () => {
    it('should put correct action with correct data', () => {
      const action = taskActions.deleteRequest(fixture.id);
      const generator = deleteTask(apiInstance, action);

      expect(generator.next().value).toEqual(call(apiInstance.deleteTask, fixture.id));
      expect(generator.next().value).toEqual(
        put(taskActions.deleteSuccess(fixture.id, fixture.task))
      );
    });

    it('should put correct action with wrong data', () => {
      const action = taskActions.deleteRequest();
      const generator = deleteTask(apiInstance);

      expect(generator.next().value).toEqual(
        put(taskActions.deleteFailure(new TypeError("Cannot read property 'id' of undefined")))
      );
    });
  });

  describe('updateTask', () => {
    it('should put correct action with correct data', () => {
      const action = taskActions.updateRequest(fixture.task);
      const generator = updateTask(apiInstance, action);

      expect(generator.next().value).toEqual(call(apiInstance.updateTasks));
      expect(generator.next([fixture.task]).value).toEqual(
        put(taskActions.updateSuccess([fixture.task]))
      );
    });

    it('should put correct action with incorrect data', async () => {
      const action = taskActions.updateRequest(fixture.task);
      const generator = updateTask(apiInstance, action);

      expect(generator.next().value).toEqual(call(apiInstance.updateTasks));
      expect(generator.next().value).toEqual(
        put(taskActions.updateFailure(new TypeError("UpdateTask: result isn't array")))
      );
    });
  });

  describe('getBaseURL', () => {
    it('should return http://localhost:3000/api', () => {
      expect(getBaseURL('development')).toBe('http://localhost:3000/api');
    });

    it('should return http://anadeaback.herokuapp.com/api', async () => {
      expect(getBaseURL('production')).toBe('https://anadea-back.herokuapp.com/api');
    });
  });
});
