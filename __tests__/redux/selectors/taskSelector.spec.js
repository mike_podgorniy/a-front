import Immutable from 'seamless-immutable';
import {
  taskIndexSelector,
  taskSortedByEditSelector,
  taskToMarkerSelector,
  taskSort,
} from '../../../src/js/redux/selectors/taskSelector';

describe('taskSelector', () => {
  const fixture = Immutable(
    [1, 2, 3, 4, 5].map(index => ({
      id: index,
      location: `location ${index}`,
      position: { lat: index, lng: index },
      serviceType: index,
      updated: index,
    }))
  );

  describe('taskIndexSelector', () => {
    it('return correct index', () => {
      expect(taskIndexSelector(fixture, 4)).toEqual(3);
    });
  });

  describe('taskSortedByEditSelector', () => {
    it('return correct array', () => {
      expect(taskSortedByEditSelector(fixture)).toEqual(fixture.asMutable().reverse());
    });
  });

  describe('taskToMarkerSelector', () => {
    it('return correct array', () => {
      const result = taskToMarkerSelector(fixture);
      expect(result[0].id).toBeDefined();
      expect(result[0].updated).toBeUndefined();
    });
  });

  describe('taskSort', () => {
    it('return correct value', () => {
      expect(taskSort(fixture[0], fixture[1])).toBe(1);
      expect(taskSort(fixture[4], fixture[1])).toBe(-1);
    });
  });
});
