import React from 'react';
import renderer from 'react-test-renderer';
import Enzyme, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import NotFoundPage from '../../src/js/views/page404';

Enzyme.configure({ adapter: new Adapter() });

describe('404View', () => {
  it('should render a correct structure with value /', () => {
    const wrapper = mount(<NotFoundPage />);
    expect(wrapper.html()).toMatchSnapshot();
  });
});
