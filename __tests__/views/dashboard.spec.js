import React from 'react';
import renderer from 'react-test-renderer';
import Enzyme, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { MemoryRouter as Router, withRouter } from 'react-router-dom'; // 4.0.0
import { createStore } from 'redux';
import Immutable from 'seamless-immutable';

import { getStore } from '../../__fixtures__/store';
import combineReducers from '../../src/js/redux/rootReducers';

import DashboardView from '../../src/js/views/dashboard';

Enzyme.configure({ adapter: new Adapter() });

const fixture = Immutable(
  [1, 2, 3, 4, 5].map(index => ({
    id: index,
    location: `location ${index}`,
    position: { lat: index, lng: index },
    serviceType: index,
    updated: index,
    description: `description $${index}`,
  }))
);

const fixtureMatch = (path, id) => ({ path, params: { id } });

describe('DashboardView', () => {
  it('should render a correct structure with value /', () => {
    const store = createStore(combineReducers);
    const wrapper = mount(
      <Router>
        <DashboardView store={store} match={fixtureMatch('/')} />
      </Router>
    );
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('should render a correct structure with value /new', () => {
    const store = createStore(combineReducers);
    const wrapper = mount(
      <Router>
        <DashboardView store={store} match={fixtureMatch('/new')} />
      </Router>
    );
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('should render a correct structure with value /edit', () => {
    const store = createStore(combineReducers, { task: { data: fixture } });
    const wrapper = mount(
      <Router>
        <DashboardView store={store} match={fixtureMatch('/edit/:id', 2)} />
      </Router>
    );
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('should call handleMarkerClick -> history.push("/edit/:id")', () => {
    const store = createStore(combineReducers, { task: { data: fixture } });
    const history = { push: jest.fn() };
    const wrapper = mount(
      <Router>
        <DashboardView store={store} history={history} match={fixtureMatch('/edit/:id', 2)} />
      </Router>
    );
    const dashboard = wrapper
      .find(DashboardView)
      .childAt(0)
      .instance();

    const spy = jest.spyOn(dashboard, 'handleMarkerClick');

    dashboard.handleMarkerClick(5);

    expect(spy).toHaveBeenCalledWith(5);
    expect(history.push.mock.calls[0][0]).toBe('/edit/5');

    spy.mockReset();
    spy.mockRestore();
  });

  it('should call handleMapClick -> props.map.click(geocoder, latLng)', () => {
    const store = createStore(combineReducers, { task: { data: fixture } });
    const history = { push: jest.fn() };
    const wrapper = mount(
      <Router>
        <DashboardView store={store} history={history} match={fixtureMatch('/edit/:id', 2)} />
      </Router>
    );
    const dashboard = wrapper
      .find(DashboardView)
      .childAt(0)
      .instance();

    const spy = jest.spyOn(dashboard.props.map, 'click');

    dashboard.handleMapClick('geocoder', 'latLng');

    expect(spy).toHaveBeenCalledWith('geocoder', 'latLng');

    spy.mockReset();
    spy.mockRestore();
  });

  it('should call handleSaveTask -> props.task.editRequest(id, data)', () => {
    const store = createStore(combineReducers, { task: { data: fixture } });
    const history = { push: jest.fn() };
    const wrapper = mount(
      <Router>
        <DashboardView store={store} history={history} match={fixtureMatch('/edit/:id', 2)} />
      </Router>
    );
    const dashboard = wrapper
      .find(DashboardView)
      .childAt(0)
      .instance();

    const spy = jest.spyOn(dashboard.props.task, 'editRequest');
    const newData = { ping: 'pong' };

    dashboard.handleSaveTask(newData);

    expect(spy).toHaveBeenCalledWith(2, newData);

    spy.mockReset();
    spy.mockRestore();
  });

  it('should call handleCreateNew -> props.task.createRequest(data) & history.push("/dashboard")', () => {
    const store = createStore(combineReducers, { task: { data: fixture } });
    const history = { push: jest.fn() };
    const wrapper = mount(
      <Router>
        <DashboardView store={store} history={history} match={fixtureMatch('/edit/:id', 2)} />
      </Router>
    );
    const dashboard = wrapper
      .find(DashboardView)
      .childAt(0)
      .instance();

    // console.log('dashboard', dashboard);
    const spy = jest.spyOn(dashboard.props.task, 'createRequest');
    const newData = { ping: 'pong' };

    dashboard.handleCreateNew(newData);

    expect(spy).toHaveBeenCalledWith(newData);
    expect(history.push.mock.calls[0][0]).toBe('/dashboard');

    spy.mockReset();
    spy.mockRestore();
  });

  it('should call history.replace("/dashboard") on delete task', () => {
    const store = createStore(combineReducers, { task: { data: fixture } });
    const history = { replace: jest.fn() };
    const wrapper = mount(
      <Router>
        <DashboardView store={store} history={history} match={fixtureMatch('/')} />
      </Router>
    );
    const dashboard = wrapper
      .find(DashboardView)
      .childAt(0)
      .instance();

    // console.log('dashboard', dashboard);
    const spy = jest.spyOn(dashboard.props.task, 'deleteRequest');

    dashboard.componentWillReceiveProps({
      history,
      match: fixtureMatch('/delete/:id', 2),
      tasks: fixture,
      task: dashboard.props.task,
    });

    expect(spy).toHaveBeenCalledWith(2);
    expect(history.replace.mock.calls[0][0]).toBe('/dashboard');

    spy.mockReset();
    spy.mockRestore();
  });

  it('should call history.replace("/404") on delete task with wrong id', () => {
    const store = createStore(combineReducers, { task: { data: fixture } });
    const history = { replace: jest.fn() };
    const wrapper = mount(
      <Router>
        <DashboardView store={store} history={history} match={fixtureMatch('/')} />
      </Router>
    );
    const dashboard = wrapper
      .find(DashboardView)
      .childAt(0)
      .instance();

    dashboard.componentWillReceiveProps({
      history,
      match: fixtureMatch('/delete/:id', 777),
      tasks: fixture,
      task: dashboard.props.task,
    });

    expect(history.replace.mock.calls[0][0]).toBe('/404');
  });

  it('should not been call history.* on edit the same task', () => {
    const store = createStore(combineReducers, { task: { data: fixture } });
    const history = { push: jest.fn(), replace: jest.fn() };
    const wrapper = mount(
      <Router>
        <DashboardView store={store} history={history} match={fixtureMatch('/edit/:id', 2)} />
      </Router>
    );
    const dashboard = wrapper
      .find(DashboardView)
      .childAt(0)
      .instance();

    dashboard.componentWillReceiveProps({
      ...dashboard.props,
      history,
      match: fixtureMatch('/edit/:id', 2),
    });

    expect(history.replace.mock.calls.length).toBe(0);
    expect(history.push.mock.calls.length).toBe(0);
  });

  it('should not been call history.* on edit the next task', () => {
    const store = createStore(combineReducers, { task: { data: fixture } });
    const history = { push: jest.fn(), replace: jest.fn() };
    const wrapper = mount(
      <Router>
        <DashboardView store={store} history={history} match={fixtureMatch('/edit/:id', 2)} />
      </Router>
    );
    const dashboard = wrapper
      .find(DashboardView)
      .childAt(0)
      .instance();

    dashboard.componentWillReceiveProps({
      ...dashboard.props,
      history,
      match: fixtureMatch('/edit/:id', 3),
    });

    expect(history.replace.mock.calls.length).toBe(0);
    expect(history.push.mock.calls.length).toBe(0);
  });
});
