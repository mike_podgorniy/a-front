import React from 'react';
import renderer from 'react-test-renderer';
import Enzyme, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { createStore } from 'redux';
import nanoid from 'nanoid';

import combineReducers from '../../src/js/redux/rootReducers';

import HistoryPage from '../../src/js/views/history';
import { logActions } from '../../src/js/redux/modules/log';
import { taskActions, taskTypes } from '../../src/js/redux/modules/task';

Enzyme.configure({ adapter: new Adapter() });

describe('HistoryPage', () => {
  const getTaskFixture = (type) => {
    const time = Date.now();
    const id = nanoid();
    const description = 'Simple description';
    const location = 'Simple description';
    return {
      id,
      task: {
        id,
        description,
        location,
        updated: time,
        created: time,
        type,
      },
      log: {
        action: {
          id,
          description: type === taskTypes.DELETE_SUCCESS ? undefined : description,
          location: type === taskTypes.DELETE_SUCCESS ? undefined : location,
          type,
        },
        at: time,
      },
    };
  };

  const getFixture = type => ({
    id: '777',
    task: { description: 'Simple description', location: 'My City', id: '777' },
    type,
  });

  it('should render a correct structure with empty log', () => {
    const store = createStore(combineReducers);
    const wrapper = mount(<HistoryPage store={store} />);
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('should call history/log reset', () => {
    const type = taskTypes.CREATE_SUCCESS;
    const fixture = getTaskFixture(type);

    const log = { reset: jest.fn() };
    const store = createStore(combineReducers);
    store.dispatch(taskActions.createSuccess(fixture.task));
    const wrapper = mount(
      <HistoryPage store={store} log={log} logEntries={[1, 2, 3, 4]} isEmpty={false} />
    );

    const component = wrapper.instance();

    // console.log('component.props', wrapper.html());
    // console.log('component.props', component.props);

    wrapper.find('.reset').simulate('click');

    // expect(log.reset.mock.calls.length).toBe(1);
  });
});
