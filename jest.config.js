module.exports = {
  rootDir: './',
  // transform: {
  //   '.*': '<rootDir>/node_modules/babel-jest',
  // },
  moduleFileExtensions: ['js', 'jsx', 'json'],
  // moduleDirectories: [
  //   'node_modules',
  //   'app/scripts',
  //   './',
  // ],
  // moduleNameMapper: {
  //   '^app-store': '<rootDir>/app/scripts/store',
  //   '^.+\\.(css|scss)$': '<rootDir>/__test__/__setup__/styleMock.js',
  //   '^(.+\\.(jpe?g|png|gif|ttf|eot|svg|md)|bootstrap.*)$': '<rootDir>/__test__/__setup__/fileMock.js',
  //   '^(expose|bundle)': '<rootDir>/test/__setup__/moduleMock.js',
  // },
  // setupFiles: [
  //   '<rootDir>/test/__setup__/shim.js',
  //   '<rootDir>/test/__setup__/index.js',
  // ],
  // setupTestFrameworkScriptFile: '<rootDir>/node_modules/jest-enzyme/lib/index.js',
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/fileMock.js',
    '\\.(css)$': 'identity-obj-proxy',
  },
  setupFiles: ['raf/polyfill'],
  automock: false,
  testRegex: '/__tests__/.*?\\.(test|spec)\\.js$',
  // testURL: 'http://localhost:3000',
  collectCoverage: false,
  collectCoverageFrom: [
    'src/js/views/**/*.{js,jsx}',
    'src/js/common/**/*.{js,jsx}',
    '!src/js/common/components/Utilities/*.{js,jsx}',
    '!src/js/common/components/Map/*.{js,jsx}',
    'src/js/redux/modules/**/*.{js,jsx}',
    'src/js/redux/sagas/**/*.{js,jsx}',
    'src/js/redux/selectors/**/*.{js,jsx}',
    // 'src/**/*.{js,jsx}',
    // '!app/scripts/vendor/*',
  ],
  coverageThreshold: {
    global: {
      branches: 90,
      functions: 90,
      lines: 90,
      statements: 90,
    },
  },
  verbose: true,
};
