const express = require('express');
const serveStatic = require('serve-static');

const app = express();

console.log(require('fs').existsSync(`${__dirname}/docroot`));

const port = process.env.PORT || 5000;
console.log(`Listening on ${port}`);

app.use(serveStatic('docroot', { index: ['index.html', 'index.htm'] }));
app.listen(port);
