import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import FacebookAuth from 'react-facebook-auth';

import './Header.css';

const items = [
  {
    title: 'DashBoard',
    paths: ['', 'dashboard', 'new', 'edit'],
  },
  'History',
  'Page',
];

const FacebookButton = ({ onClick }) => (
  <button onClick={onClick} className="right">
    Login with facebook
  </button>
);

class Header extends PureComponent {
  state = {
    fbAuth: false,
  };

  handleAuthenticate = (response) => {
    // Api call to server so we can validate the token
    console.log('authenticate', response);
    const { name, picture: { data: { url } } } = response;
    this.setState({
      fbAuth: true,
      fbName: name,
      fbAvatar: url,
    });
  };

  render() {
    const { location: { pathname } } = this.props;
    const path = pathname.slice(1).split('/')[0];
    const { fbAuth, fbName, fbAvatar } = this.state;
    return (
      <header className="globalHeader">
        <span className="logo">JobUp</span>
        <ul>
          {items.map((item) => {
            let isActive;
            let title;

            if (typeof item === 'string') {
              isActive = path === item.toLowerCase();
              title = item;
            } else {
              isActive = item.paths.indexOf(path) !== -1;
              title = item.title;
            }

            return (
              <li key={title} className={isActive ? 'active' : ''}>
                <Link to={`/${title.toLowerCase()}`}>{title}</Link>
              </li>
            );
          })}
        </ul>
        {fbAuth ? (
          <div className="header__fb">
            <img src={fbAvatar} alt={fbName} className="header__fb__avatar" /> {fbName}
          </div>
        ) : (
          <FacebookAuth
            appId="383302342117843"
            callback={this.handleAuthenticate}
            component={FacebookButton}
          />
        )}
      </header>
    );
  }
}

export default Header;
