import React from 'react';
import { compose, withProps, withHandlers } from 'recompose';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps';
import serviceTypes from '../Task/ServiceTypes';

const getIcon = index => serviceTypes[index].icon;

const GMarker = compose(
  withHandlers(() => ({
    onClick: props => () => {
      props.onClick(props.id);
    },
  }))
)(props => (
  <Marker
    position={props.position}
    defaultIcon={getIcon(props.serviceType)}
    onClick={props.onClick}
  />
));

const GMap = compose(
  withProps({
    googleMapURL:
      'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyCrJOrKd1Caedst-6LKkNK3HSghY25TgaM',
    loadingElement: <div style={{ height: '100%', width: '100%', display: 'flex' }} />,
    containerElement: <div style={{ height: '100%', width: '100%', display: 'flex' }} />,
    mapElement: <div style={{ height: '100%', width: '100%', display: 'flex' }} />,
  }),
  withHandlers(() => {
    const refs = {
      map: undefined,
      geocoder: undefined,
    };

    return {
      handleMapMounted: () => (ref) => {
        refs.map = ref;
        refs.geocoder = new google.maps.Geocoder(); // eslint-disable-line
      },

      handleMapClick: ({ onMapClick }) => (event) => {
        if (onMapClick) {
          const { latLng: { lat, lng } } = event;
          onMapClick(refs.geocoder, { lat: lat(), lng: lng() });
        }
      },
    };
  }),
  withScriptjs,
  withGoogleMap
)(props => (
  <GoogleMap
    defaultOptions={{ disableDefaultUI: true }}
    defaultZoom={14}
    defaultCenter={{ lat: 48.46950694703277, lng: 35.048789978027344 }}
    ref={props.handleMapMounted}
    onClick={props.handleMapClick}
  >
    {props.markers.map(({ id, location, position, serviceType }) => (
      <GMarker
        id={id}
        key={`${id}_${serviceType}_${location}`}
        position={position}
        serviceType={serviceType}
        onClick={props.onMarkerClick}
      />
    ))}
    {props.mapLastPosition && <Marker position={props.mapLastPosition.latLng} />}
  </GoogleMap>
));

export default GMap;
