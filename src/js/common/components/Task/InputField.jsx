import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import './Task.css';

class InputField extends PureComponent {
  handleFocus = () => {
    this.field.focus();
  };

  render() {
    const { value, title, onChange, hint, disabled } = this.props;
    return (
      <div className="task__field" onClick={this.handleFocus} role="textbox" tabIndex="0">
        <h2>{title}</h2>
        <input
          disabled={disabled}
          ref={(ref) => {
            this.field = ref;
          }}
          onChange={onChange}
          value={value}
        />
        <div className="task__field__hint">{value ? '' : <em>{hint}</em>}</div>
      </div>
    );
  }
}

InputField.defaultProps = {
  value: '',
  disabled: false,
  onChange: null,
};

InputField.propTypes = {
  disabled: PropTypes.bool,
  value: PropTypes.string,
  hint: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  onChange: PropTypes.func,
};

export default InputField;
