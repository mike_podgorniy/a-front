import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './Task.css';

class ServiceTypeItem extends PureComponent {
  handleOnClick = () => {
    const { onPress, id } = this.props;
    onPress(id);
  };

  render() {
    const { id, title, icon, selected } = this.props;
    return (
      <span
        className="task__serviceType__item"
        onClick={this.handleOnClick}
        tabIndex={id}
        role="button"
      >
        <span className={`task__serviceType__image ${selected ? 'selected' : ''}`}>
          <img src={icon} alt={title} />
        </span>
        {title}
      </span>
    );
  }
}

ServiceTypeItem.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  selected: PropTypes.bool.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default ServiceTypeItem;
