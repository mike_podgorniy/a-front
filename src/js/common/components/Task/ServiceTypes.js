const cookIcon = require('./assets/icons/cook.svg');
const electricianIcon = require('./assets/icons/electrician.svg');
const plumberIcon = require('./assets/icons/plumber.svg');
const gardenerIcon = require('./assets/icons/gardener.svg');
const housekeeperIcon = require('./assets/icons/housekeeper.svg');

const serviceTypes = [
  {
    id: 0,
    title: 'Electrician',
    icon: electricianIcon,
    tasks: [
      'Unblock a toilet',
      'Unblock a sink',
      'Fix a water leak',
      'Install a sink',
      'Install a shower',
      'Install a toilet',
    ],
  },
  {
    id: 1,
    title: 'Plumber',
    icon: plumberIcon,
    tasks: [
      '1Unblock a toilet',
      '1Unblock a sink',
      '1Fix a water leak',
      '1Install a sink',
      '1Install a shower',
      '1Install a toilet',
    ],
  },
  {
    id: 2,
    title: 'Gardener',
    icon: gardenerIcon,
    tasks: [
      '2Unblock a toilet',
      '2Unblock a sink',
      '2Fix a water leak',
      '2Install a sink',
      '2Install a shower',
      '2Install a toilet',
    ],
  },
  {
    id: 3,
    title: 'Housekeeper',
    icon: housekeeperIcon,
    tasks: [
      '3Unblock a toilet',
      '3Unblock a sink',
      '3Fix a water leak',
      '3Install a sink',
      '3Install a shower',
      '3Install a toilet',
    ],
  },
  {
    id: 4,
    title: 'Cook',
    icon: cookIcon,
    tasks: [
      '4Unblock a toilet',
      '4Unblock a sink',
      '4Fix a water leak',
      '4Install a sink',
      '4Install a shower',
      '4Install a toilet',
    ],
  },
];

export default serviceTypes;
