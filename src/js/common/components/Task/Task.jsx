import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import '../../../utility/lodash.mixin';

import ServiceTypeItem from './ServiceTypeItem';
import TaskItem from './TaskItem';
import InputField from './InputField';
import './Task.css';
import serviceTypes from './ServiceTypes';

const addAorAn = part => (part ? `${part.match(/^[aeiou]/i) ? 'an' : 'a'} ${part}` : '');

const prepareDescriptionPart = (part, prefix = '', suffix = '') =>
  (part ? `${_.escape(prefix)}<strong>${_.escape(part)}</strong>${_.escape(suffix)}` : '...');

export const prepareDescription = (description) => {
  if (typeof description !== 'string') {
    return description.map(
      value =>
        (value.includes('<strong>') ? (
          <strong key={value}>{value.replace(/<\/?strong>/gi, '')}</strong>
        ) : (
          value
        ))
    );
  }
  return '';
};

class Task extends PureComponent {
  constructor(props) {
    super(props);

    if (props.task) {
      this.state = this.setPreviewDescription(this.updateStateFromProps(props), false);
    } else {
      this.state = {
        serviceType: undefined,
        tasks: undefined,
        taskDescription: '',
        location: '',
        position: undefined,
        description: ['I need...'],
      };
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.mapLastPosition && nextProps.mapLastPosition !== this.props.mapLastPosition) {
      const { address, latLng } = nextProps.mapLastPosition;
      // this.setState({
      //   location: address,
      // });
      this.setPreviewDescription({
        location: address,
        position: latLng,
      });
    }
    if (nextProps.task && nextProps.task !== this.props.task) {
      this.setPreviewDescription(this.updateStateFromProps(nextProps));
    }
  }

  setPreviewDescription = (nextState, updateState = true) => {
    const combinedState = {
      ...this.state,
      ...nextState,
    };
    const { serviceType, tasks, taskDescription, location } = combinedState;
    const isServiceTypeSelect = serviceType !== undefined;
    const selectedService = isServiceTypeSelect ? serviceTypes[serviceType] : null;
    const isTasksSelect = tasks !== undefined && tasks.length;
    const isTaskDescriptionFill = _.trim(taskDescription).length > 0;
    const isLocationFill = _.trim(location).length > 0;
    const isTaskComplete =
      isServiceTypeSelect && isTasksSelect && isTaskDescriptionFill && isLocationFill;

    const description = [
      'I need',
      isServiceTypeSelect && isServiceTypeSelect
        ? prepareDescriptionPart(addAorAn(selectedService.title.toLowerCase()), ' ', '')
        : '',
      isTasksSelect
        ? tasks
          .map((item, index) => {
            const title = selectedService.tasks[item].toLowerCase();
            return prepareDescriptionPart(title, index === 0 ? ' to ' : ', to ', '');
          })
          .join('')
        : '',
      taskDescription ? prepareDescriptionPart(taskDescription.toLowerCase(), ', ', '') : '',
      isTaskComplete ? '.' : '...',
    ];

    const finalState = { ...combinedState, description, isTaskComplete };

    if (updateState) {
      this.setState(finalState);
    } else {
      return finalState;
    }
  };

  updateStateFromProps = props => ({
    ...props.task,
    isTaskComplete: true,
  });

  handleServiceTypeSelect = (id) => {
    const { serviceType } = this.state;

    if (serviceType !== id) {
      this.setPreviewDescription({
        // isTaskComplete: false,
        serviceType: id,
        tasks: [],
      });
    }
  };

  handleTaskItemSelect = (id) => {
    const { tasks } = this.state;
    this.setPreviewDescription({
      tasks: _.toggle(tasks, id),
    });
  };

  handleChangeDescription = ({ target: { value } }) =>
    this.setPreviewDescription({
      taskDescription: value,
    });

  handleCompleteEdit = (e) => {
    e.preventDefault();

    const { onTask } = this.props;
    const {
      serviceType,
      tasks,
      taskDescription,
      location,
      description,
      position,
      created,
    } = this.state;

    onTask({
      serviceType,
      tasks,
      taskDescription,
      location,
      position,
      created,
      description: description.join(''),
    });
  };

  render() {
    const { previewTitle, actionUrl, actionTitle } = this.props;
    const {
      serviceType,
      tasks,
      description,
      taskDescription,
      location,
      isTaskComplete,
    } = this.state;

    return (
      <div className="task">
        <div className="task__container">
          <div className="task__preview">
            <h2>{previewTitle}</h2>
            <div className="task__preview__description">{prepareDescription(description)}</div>
            {location && <div className="task__preview__address">My Address: {location}</div>}
            {isTaskComplete ? (
              <div className="task__preview__createtask">
                <a href={`#${actionUrl}`} onClick={this.handleCompleteEdit}>
                  {actionTitle}
                </a>
              </div>
            ) : (
              <em>Please fill in all fields</em>
            )}
          </div>
          <InputField
            disabled
            title="Localtion"
            value={location}
            hint="Set your location on the map"
          />
          <div className="task__field">
            <h2>Service type</h2>
            <div className="task__serviceType">
              {serviceTypes.map(item => (
                <ServiceTypeItem
                  key={item.id}
                  selected={item.id === serviceType}
                  onPress={this.handleServiceTypeSelect}
                  {...item}
                />
              ))}
            </div>
          </div>
          <div className="task__field">
            <h2>{tasks && serviceTypes[serviceType].title} Tasks</h2>
            {tasks === undefined ? (
              <em>Select a service type</em>
            ) : (
              serviceTypes[serviceType].tasks.map((task, index) => (
                <TaskItem
                  key={task}
                  id={index}
                  title={task}
                  selected={tasks.indexOf(index) !== -1}
                  onPress={this.handleTaskItemSelect}
                />
              ))
            )}
          </div>
          <InputField
            title="Task Description"
            onChange={this.handleChangeDescription}
            value={taskDescription}
            hint="Enter task description"
          />
        </div>
      </div>
    );
  }
}

Task.defaultProps = {
  previewTitle: 'New Task',
  actionUrl: '/create',
  actionTitle: 'Create Task',
  mapLastPosition: null,
  task: null,
};

Task.propTypes = {
  previewTitle: PropTypes.string,
  actionUrl: PropTypes.string,
  actionTitle: PropTypes.string,
  onTask: PropTypes.func.isRequired,
  mapLastPosition: PropTypes.object,
  task: PropTypes.object,
};

export default Task;

export const EditTask = props => (
  <Task previewTitle="Edit Task" actionUrl="/save" actionTitle="Save" {...props} />
);
