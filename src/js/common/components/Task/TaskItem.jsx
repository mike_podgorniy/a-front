import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class TaskItem extends PureComponent {
  handleOnClick = () => {
    const { onPress, id } = this.props;
    onPress(id);
  };

  render() {
    const { title, id, selected } = this.props;
    return (
      <div
        className={`task__taskTag__item ${selected ? 'selected' : ''}`}
        onClick={this.handleOnClick}
        tabIndex={id}
        role="button"
      >
        <span>{title}</span>
      </div>
    );
  }
}

TaskItem.propTypes = {
  title: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  selected: PropTypes.bool.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default TaskItem;
