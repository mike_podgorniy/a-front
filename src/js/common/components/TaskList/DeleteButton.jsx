import React, { PureComponent } from 'react';
import { ModalContainer, ModalDialog } from 'react-modal-dialog';
import { Link } from 'react-router-dom';

import './TaskList.css';

class DeleteButton extends PureComponent {
  state = {
    isShowingModal: false,
  };

  handleClick = (e) => {
    e.preventDefault();
    this.setState({ isShowingModal: true });
  };

  handleClose = () => this.setState({ isShowingModal: false });
  render() {
    const { link } = this.props;
    return (
      <Link to={link} onClick={this.handleClick} className="button white">
        Delete
        {this.state.isShowingModal && (
          <ModalContainer onClose={this.handleClose}>
            <ModalDialog onClose={this.handleClose}>
              <h1>Delete Task</h1>
              <p>Are you sure you want to delete this task?</p>
              <div className="taskList__modal_delete__buttons">
                <Link to={link} className="button">
                  Delete
                </Link>
              </div>
            </ModalDialog>
          </ModalContainer>
        )}
      </Link>
    );
  }
}

export default DeleteButton;
