import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import moment from 'moment';

import './TaskList.css';
import DeleteButton from './DeleteButton';

class TaskList extends PureComponent {
  render() {
    const { tasks } = this.props;

    return (
      <div className="taskList">
        <div className="taskList__container">
          <div className="taskList__link">
            <Link to="/new">+ New Task</Link>
          </div>
          {tasks.map((item) => {
            const { id, created, description } = item;
            return (
              <div className="taskList__task" key={id}>
                <div className="taskList__task__created">
                  {moment(new Date(created)).calendar()}
                </div>
                <div className="taskList__task__description">
                  {description.replace(/<\/?strong>/gi, '')}
                </div>
                <div>
                  <Link to={`/edit/${id}`} className="button">
                    Edit
                  </Link>
                  <DeleteButton link={`/delete/${id}`} />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

TaskList.propTypes = {
  tasks: PropTypes.array.isRequired,
};

export default TaskList;
