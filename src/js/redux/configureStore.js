import createSagaMiddleware from 'redux-saga';
import { routerMiddleware } from 'react-router-redux';
import { applyMiddleware, compose, createStore } from 'redux';
import { autoRehydrate } from 'redux-persist';
import ReduxPersist from '../services/ReduxPersist';
import RehydrationServices from '../services/RehydrationServices';

import sagas from './sagas';
import rootReducer from './rootReducers';

// Redux DevTools Extension for Chrome and Firefox
const reduxDevTool = () => {
  return typeof window === 'object' && typeof window.devToolsExtension !== 'undefined'
    ? window.devToolsExtension()
    : f => f;
};

export default function configureStore(initialState, history) {
  const enhancers = [];
  const sagaMiddleware = createSagaMiddleware();

  // const middleware = applyMiddleware(sagaMiddleware, routerMiddleware(history));
  enhancers.push(applyMiddleware(sagaMiddleware, routerMiddleware(history)));

  // add the autoRehydrate enhancer
  if (ReduxPersist.active) {
    enhancers.push(autoRehydrate());
  }

  enhancers.push(reduxDevTool());

  const composedStoreEnhancer = compose(...enhancers);

  const store = composedStoreEnhancer(createStore)(rootReducer, initialState);

  // configure persistStore and check reducer version number
  if (ReduxPersist.active) {
    RehydrationServices.updateReducers(store);
  }

  sagaMiddleware.run(sagas);

  if (module.hot) {
    module.hot.accept('./rootReducers', () => {
      store.replaceReducer(require('./rootReducers'));
    });
  }

  return store;
}
