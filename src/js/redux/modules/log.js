import Immutable from 'seamless-immutable';
import { createReducer, createActions } from 'reduxsauce';
import { taskTypes } from './task';

const { Types, Creators } = createActions(
  {
    reset: null,
  },
  { prefix: 'LOG_' }
);

export const logActions = Creators;
export const logTypes = Types;

// the initial state of this reducer
const INITIAL_STATE = Immutable([]);

const logReset = () => INITIAL_STATE;

const logAction = (state, action) => {
  const { id, task, type } = action;
  const { description, location, id: taskID } = task || {};

  return Immutable(
    [
      {
        at: Date.now(),
        action: {
          id: taskID || id,
          type,
          description,
          location,
        },
      },
    ].concat(state)
  );
};

// map our action types to our reducer functions
export const HANDLERS = {
  [Types.RESET]: logReset,
  [taskTypes.CREATE_SUCCESS]: logAction,
  [taskTypes.EDIT_SUCCESS]: logAction,
  [taskTypes.DELETE_SUCCESS]: logAction,
};

export default createReducer(INITIAL_STATE, HANDLERS);
