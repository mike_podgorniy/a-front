import Immutable from 'seamless-immutable';
import { createReducer, createActions } from 'reduxsauce';
import { taskTypes } from './task';

const { Types, Creators } = createActions(
  {
    click: ['geocoder', 'latLng'],
    changeLastPosition: ['marker'],
    error: ['error'],
  },
  { prefix: 'MAP_' }
);

export const mapActions = Creators;
export const mapTypes = Types;

// the initial state of this reducer
export const INITIAL_STATE = Immutable({
  lastPosition: null,
});

const changeLastPosition = (state, { marker }) => state.set('lastPosition', marker);

const clearLastPosition = state => state.set('lastPosition', null);

// map our action types to our reducer functions
export const HANDLERS = {
  [Types.CHANGE_LAST_POSITION]: changeLastPosition,
  [taskTypes.CREATE_SUCCESS]: clearLastPosition,
  [taskTypes.EDIT_SUCCESS]: clearLastPosition,
  [taskTypes.DELETE_SUCCESS]: clearLastPosition,
};

export default createReducer(INITIAL_STATE, HANDLERS);
