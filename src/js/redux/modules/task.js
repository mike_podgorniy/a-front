import Immutable from 'seamless-immutable';
import { createReducer, createActions } from 'reduxsauce';
import { taskIndexSelector } from '../selectors/taskSelector';

const { Types, Creators } = createActions(
  {
    createRequest: ['task'],
    createSuccess: ['task'],
    createFailure: ['error'],
    editRequest: ['id', 'task'],
    editSuccess: ['id', 'task'],
    editFailure: ['error'],
    deleteRequest: ['id'],
    deleteSuccess: ['id'],
    deleteFailure: ['error'],
    edit: ['task'],
    updateRequest: null,
    updateSuccess: ['tasks'],
    updateFailure: ['error'],
  },
  { prefix: 'TASK_' }
);

export const taskActions = Creators;
export const taskTypes = Types;

// the initial state of this reducer
export const INITIAL_STATE = Immutable({
  data: [],
});

const createSuccess = (state, { task }) => {
  return state.set('data', [task].concat(state.data));
};

const editSuccess = (state, { id, task }) => {
  const index = taskIndexSelector(state.data, id);
  const oldTask = state.getIn(['data', index], task);
  return state.setIn(['data', index], { ...oldTask, ...task });
};

const deleteSuccess = (state, { id }) => {
  const index = taskIndexSelector(state.data, parseInt(id, 10));
  return state.setIn(['data'], state.data.filter((item, itemIndex) => index !== itemIndex));
};

const updateSuccess = (state, { tasks }) => {
  return state.setIn(['data'], tasks);
};

// map our action types to our reducer functions
export const HANDLERS = {
  [Types.CREATE_SUCCESS]: createSuccess,
  [Types.EDIT_SUCCESS]: editSuccess,
  [Types.DELETE_SUCCESS]: deleteSuccess,
  [Types.UPDATE_SUCCESS]: updateSuccess,
};

export default createReducer(INITIAL_STATE, HANDLERS);
