import { persistStore, persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/es/storage'; // default: localStorage if web, AsyncStorage if react-native
import { createStore } from 'redux';
import reducers from './rootReducers'; // where reducers is an object of reducers

const config = {
  key: 'root',
  storage,
};

const reducer = persistCombineReducers(config, reducers);

export default function configureStore() {
  // ...
  console.log('configureStore PERSIST!!! ')
  const store = createStore(reducer);
  const persistor = persistStore(store);

  return { persistor, store };
}
