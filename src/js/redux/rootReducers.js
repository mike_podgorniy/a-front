import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import task from './modules/task';
import map from './modules/map';
import log from './modules/log';

export default combineReducers({
  task,
  map,
  log,
  routing,
});
