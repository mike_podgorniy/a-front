import { all, fork } from 'redux-saga/effects';
import taskSaga from './taskSaga';
import mapSaga from './mapSaga';

export default function* root() {
  yield all([fork(taskSaga), fork(mapSaga)]);
}
