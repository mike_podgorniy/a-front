import { all, put, call, takeLatest } from 'redux-saga/effects';
import { mapActions, mapTypes as types } from '../modules/map';

export const requestWrapper = (method, ...args) => {
  const params = [...args];

  return new Promise((resolve, reject) => {
    if (method && typeof method === 'function') {
      params.push((results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            const { address_components: addressComponents } = results[0];
            const shortAddress = addressComponents
              .slice(0, 4)
              .map(address => address.short_name)
              .join(', ');

            return resolve(shortAddress);
          }
          reject('No results found');
        } else {
          reject(`Geocoder failed due to: ${status}`);
        }
        return false;
      });
      method.apply(this, params);
    } else {
      reject(new Error('not a function'));
    }
  });
};

export function* changeLastPosition(action) {
  try {
    const { geocoder, latLng } = action;
    const address = yield call(requestWrapper, geocoder.geocode, { location: latLng });
    yield put(mapActions.changeLastPosition({ latLng, address }));
  } catch (error) {
    yield put(mapActions.error(error));
  }
}

export default function* root() {
  yield all([yield takeLatest(types.CLICK, changeLastPosition)]);
}
