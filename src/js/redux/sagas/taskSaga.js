import { all, put, call, takeLatest } from 'redux-saga/effects';
import { REHYDRATE } from 'redux-persist/constants';
import { taskActions, taskTypes as types } from '../modules/task';

import Api from '../../services/Api';

export const getBaseURL = env =>
  (env === 'development' ? 'http://localhost:3000/api' : 'https://anadea-back.herokuapp.com/api');

export const apiInstance = Api.getInstance(getBaseURL(process.env.NODE_ENV));

console.log('process.env.NODE_ENV', process.env.NODE_ENV);

export function* createTask(api: Api, action) {
  try {
    const { task } = action;
    const response = yield call(api.createTask, task);
    yield put(taskActions.createSuccess(response));
    yield put(taskActions.updateRequest());
  } catch (error) {
    yield put(taskActions.createFailure(error));
  }
}

export function* editTask(api: Api, action) {
  try {
    const { task, id } = action;
    const response = yield call(api.editTask, id, task);
    yield put(taskActions.editSuccess(id, response));
  } catch (error) {
    yield put(taskActions.editFailure(error));
  }
}

export function* deleteTask(api: Api, action) {
  try {
    const { id } = action;
    yield call(api.deleteTask, id);
    yield put(taskActions.deleteSuccess(id));
  } catch (error) {
    yield put(taskActions.deleteFailure(error));
  }
}

export function* updateTask(api: Api) {
  try {
    const result = yield call(api.updateTasks);
    if (Array.isArray(result)) {
      yield put(taskActions.updateSuccess(result));
    } else {
      throw new TypeError("UpdateTask: result isn't array");
    }
  } catch (error) {
    yield put(taskActions.updateFailure(error));
  }
}

export default function* root() {
  yield all([yield takeLatest(types.CREATE_REQUEST, createTask, apiInstance)]);
  yield all([yield takeLatest(types.EDIT_REQUEST, editTask, apiInstance)]);
  yield all([yield takeLatest(types.DELETE_REQUEST, deleteTask, apiInstance)]);
  yield all([yield takeLatest([types.UPDATE_REQUEST, REHYDRATE], updateTask, apiInstance)]);
}
