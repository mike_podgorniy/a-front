import { createSelector } from 'reselect';

export const taskIndexSelector = createSelector(
  tasks => tasks,
  (_, id) => id,
  (tasks, id) => tasks.findIndex(task => task.id === id)
);

export const taskSort = (taskA, taskB) => (taskA.updated > taskB.updated ? -1 : 1);
export const taskSortedByEditSelector = createSelector(
  tasks => tasks,
  tasks => tasks.asMutable().sort(taskSort)
);

export const taskToMarkerSelector = createSelector(
  tasks => tasks,
  (tasks) => {
    return tasks.map(({ id, location, position, serviceType }) => ({
      id,
      location,
      position,
      serviceType,
    }));
  }
);
