import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import { Header } from './common/components/Header';
import DashboardRouteHandler from './views/dashboard';
import HistoryRouteHandler from './views/history';
import Page404 from './views/page404';

// import '../assets/fonts/fonts.css';

const JustAnotherPage = () => (
  <div>
    <h2>This is Just Another Page</h2>
    <p>Please remove this from your route, it is just to show case basic setup for router.</p>
  </div>
);

const HeaderWithRouter = withRouter(props => <Header {...props} />);

module.exports = (
  <div className="container">
    <HeaderWithRouter />
    <div className="container__content">
      <Switch>
        <Route exact path="/" component={DashboardRouteHandler} />
        <Route path="/dashboard" component={DashboardRouteHandler} />
        <Route path="/new" component={DashboardRouteHandler} />
        <Route path="/edit/:id" component={DashboardRouteHandler} />
        <Route path="/delete/:id" component={DashboardRouteHandler} />
        <Route path="/history" component={HistoryRouteHandler} />
        <Route path="404" component={Page404} />
        <Route component={Page404} />
      </Switch>
    </div>
  </div>
);
