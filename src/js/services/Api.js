import nanoid from 'nanoid';

// TODO: Прикрутить апи к бекенду

class Api {
  static instance = null;

  static getInstance(baseUrl): Api {
    if (!Api.instance) {
      Api.instance = new Api(baseUrl);
    } else if (baseUrl) {
      Api.instance.setBaseUrl(baseUrl);
    }
    return Api.instance;
  }

  constructor(baseUrl) {
    this.setBaseUrl(baseUrl);
  }

  setBaseUrl = (baseUrl) => {
    this.baseUrl = baseUrl;
  };

  createRequest = async (url, config = {}) => {
    const response = await fetch(`${this.baseUrl}/${url}`, config);
    const data = await response.json();
    return data;
  };

  createHeaderRequest = (url, body = {}, method) =>
    this.createRequest(url, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      method,
      body: JSON.stringify(body),
    });

  createPostRequest = (url, body) => this.createHeaderRequest(url, body, 'POST');
  createPutRequest = (url, body) => this.createHeaderRequest(url, body, 'PUT');
  createDeleteRequest = (url, body) => this.createHeaderRequest(url, body, 'DELETE');

  updateTasks = () => this.createRequest('Tasks');

  createTask = (task) => {
    const createdTask = this.prepareResult(task);
    return this.createPostRequest(
      'Tasks',
      this.prepareResult({
        ...createdTask,
        created: createdTask.updated,
      })
    );
  };

  editTask = (id, task) => this.createPutRequest(`Tasks/${id}`, this.prepareResult(task));

  deleteTask = async (id) => {
    const { count } = await this.createDeleteRequest(`Tasks/x${id}`);
    return count === 1;
  };

  /* eslint-disable class-methods-use-this  */
  prepareResult = (result) => {
    return {
      ...result,
      updated: Date.now(),
    };
  };
  /* eslint-enable class-methods-use-this  */
}

export default Api;
