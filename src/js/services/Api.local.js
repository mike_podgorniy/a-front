import nanoid from 'nanoid';

// TODO: Прикрутить апи к бекенду

class Api {
  static instance = null;

  static getInstance(baseUrl): Api {
    if (!Api.instance) {
      Api.instance = new Api(baseUrl);
    } else if (baseUrl) {
      Api.instance.setBaseUrl(baseUrl);
    }
    return Api.instance;
  }

  constructor(baseUrl) {
    this.setBaseUrl(baseUrl);
  }

  setBaseUrl = (baseUrl) => {
    this.baseUrl = baseUrl;
  };

  createTask = (task) => {
    const createdTask = this.prepareResult(task);

    return {
      ...createdTask,
      id: nanoid(),
      created: createdTask.updated,
    };
  };

  editTask = (id, task) => {
    return this.prepareResult({
      ...task,
      id,
    });
  };

  deleteTask = (id) => {
    return true;
  };

  /* eslint-disable class-methods-use-this  */
  prepareResult = (result) => {
    return {
      ...result,
      updated: Date.now(),
    };
  };
  /* eslint-enable class-methods-use-this  */
}

export default Api;
