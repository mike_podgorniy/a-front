import { persistStore } from 'redux-persist';
import ReduxPersist from './ReduxPersist';

const updateReducers = (store) => {
  const { reducerVersion } = ReduxPersist;
  const config = ReduxPersist.storeConfig;

  // Check to ensure latest reducer version
  const localVersion = localStorage.getItem('reducerVersion');

  try {
    if (localVersion !== reducerVersion) {
      console.log({
        name: 'PURGE',
        value: {
          'Old Version:': localVersion,
          'New Version:': reducerVersion,
        },
        preview: 'Reducer Version Change Detected',
        important: true,
      });
      // Purge store
      persistStore(store, config).purge();
      localStorage.setItem('reducerVersion', reducerVersion);
    } else {
      persistStore(store, config);
    }
  } catch (error) {
    persistStore(store, config);
    localStorage.setItem('reducerVersion', reducerVersion);
  }
};

export default { updateReducers };
