import _ from 'lodash';

_.mixin({
  deep(obj, mapper) {
    return mapper(
      _.mapValues(obj, v => {
        if (_.isArray(v)) {
          return _.map(v, w => {
            if (_.isString(w) || _.isNumber(w)) {
              return w;
            }
            return _.deep(w, mapper);
          });
        }
        return _.isPlainObject(v) ? _.deep(v, mapper) : v;
      })
    );
  },
});

_.mixin({
  log(data) {
    console.log(JSON.stringify(data, null, 2)); //eslint-disable-line
  },
});

_.mixin({
  toCamelCaseDeep(data) {
    return _.deep(data, x => _.mapKeys(x, (val, key) => _.camelCase(key)));
  },
});

_.mixin({
  toggle(collection, item) {
    const index = _.indexOf(collection, item);
    if (index !== -1) {
      return _.without(collection, item);
    }
    return [...collection, item];
  },
});

_.mixin({
  prepareErrors(data) {
    const errors = [];
    errors[0] = _.keys(data);
    errors[1] = data;
    return errors;
  },
});

_.mixin({
  nbsp(data) {
    return data.replace(/&nbsp;/g, ' '); //nbsp;
  },
});

_.mixin({
  getError(data, errorID) {
    const [keys, errors] = data;
    if (keys && keys.indexOf(errorID) > -1) {
      return errors[errorID];
    }
    return undefined;
  },
});

_.mixin({
  getAllErrors(data) {
    const [keys, errors] = data;
    return keys.reduce((message, error) => {
      const values = _.values(errors[error]).map(errorItem => `${errorItem}. `);
      return `${message}${values}`;
    }, '');
  },
});

/*eslint-disable no-param-reassign */
_.mixin({
  objectToQueryString(obj) {
    const qs = _.reduce(
      obj,
      (result, value, key) => {
        if (!_.isNull(value) && !_.isUndefined(value)) {
          if (_.isArray(value)) {
            result += _.reduce(
              value,
              (result1, value1) => {
                if (!_.isNull(value1) && !_.isUndefined(value1)) {
                  result1 += `${key}=${value1}&`;
                  return result1;
                }
                return result1;
              },
              ''
            );
          } else {
            result += `${key}=${value}&`;
          }
          return result;
        }
        return result;
      },
      ''
    ).slice(0, -1);
    return qs;
  },
});
/*eslint-enable no-param-reassign */

/**
 * Склонение существительных
 * Правильная форма cуществительного рядом с числом (счетная форма).
 *
 * @example declension("файл", "файлов", "файла", 0);//returns "файлов"
 * @example declension("файл", "файлов", "файла", 1);//returns "файл"
 * @example declension("файл", "файлов", "файла", 2);//returns "файла"
 *
 * @param {string} oneNominative единственное число (именительный падеж)
 * @param {string} severalGenitive множественное число (родительный падеж)
 * @param {string} severalNominative множественное число (именительный падеж)
 * @param {(string|number)} number количество
 * @returns {string}
 */

/*eslint-disable no-param-reassign */
/*eslint-disable no-return-assign */
/*eslint-disable no-nested-ternary */
/*eslint-disable no-cond-assign */

_.mixin({
  declension(oneNominative, severalGenitive, severalNominative, number) {
    number %= 100;

    return number <= 14 && number >= 11
      ? severalGenitive
      : (number %= 10) < 5
        ? number > 2
          ? severalNominative
          : number === 1 ? oneNominative : number === 0 ? severalGenitive : severalNominative //number === 2
        : severalGenitive;
  },
});
/*eslint-enable no-cond-assign */
/*eslint-enable no-return-assign */
/*eslint-enable no-nested-ternary */
/*eslint-enable no-param-reassign */
