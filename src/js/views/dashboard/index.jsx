import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { taskActions } from '../../redux/modules/task';
import { mapActions } from '../../redux/modules/map';
// import { exampleSelector } from '../../redux/selectors/exampleSelector';
import GMap from '../../common/components/Map';
import TaskList from '../../common/components/TaskList';
import Task, { EditTask } from '../../common/components/Task';
import {
  taskIndexSelector,
  taskSortedByEditSelector,
  taskToMarkerSelector,
} from '../../redux/selectors/taskSelector';

require('../../../style/index.css');

const mapStateToProps = ({ map, task }) => ({
  // example: exampleSelector(state),
  mapLastPosition: map.lastPosition,
  tasks: taskSortedByEditSelector(task.data),
  markers: taskToMarkerSelector(task.data),
});

const mapDispatchToProps = dispatch => ({
  task: bindActionCreators(taskActions, dispatch),
  map: bindActionCreators(mapActions, dispatch),
});

@connect(mapStateToProps, mapDispatchToProps)
class DashboardView extends Component {
  static propTypes = {
    map: PropTypes.object.isRequired,
    task: PropTypes.object.isRequired,
    tasks: PropTypes.array.isRequired,
    mapLastPosition: PropTypes.object,
  };

  static defaultProps = {
    mapLastPosition: null,
  };

  componentWillReceiveProps(nextProps) {
    const { history, match: { path, params: { id } } } = nextProps;

    if (path === '/edit/:id') {
      const { match: { params: { id: currentId } } } = this.props;

      if (id !== currentId) {
        const { tasks } = nextProps;
        const taskIndex = taskIndexSelector(tasks, parseInt(id, 10));

        console.log({ taskIndex });
        if (taskIndex === -1) {
          history.replace('/404');
        }
      }
    } else if (path === '/delete/:id') {
      const { tasks } = nextProps;
      const taskIndex = taskIndexSelector(tasks, parseInt(id, 10));

      if (taskIndex !== -1) {
        const { task } = nextProps;
        task.deleteRequest(parseInt(id, 10));
        history.replace('/dashboard');
      } else {
        history.replace('/404');
      }
    }
  }

  handleCreateNew = (data) => {
    const { task, history } = this.props;
    task.createRequest(data);
    history.push('/dashboard');
  };

  handleSaveTask = (data) => {
    const { task, match: { params: { id } } } = this.props;
    task.editRequest(parseInt(id), data);
  };

  handleMapClick = (geocoder, latLng) => {
    const { map } = this.props;
    map.click(geocoder, latLng);
  };

  handleMarkerClick = (taskId) => {
    const { history } = this.props;
    history.push(`/edit/${taskId}`);
  };

  render() {
    const { match: { path }, mapLastPosition, tasks, markers } = this.props;
    const isCreateNew = path === '/new';
    const isEdit = path === '/edit/:id';
    let taskIndex;

    if (isEdit) {
      const { match: { params: { id } } } = this.props;
      taskIndex = taskIndexSelector(tasks, parseInt(id, 10));
    }

    return (
      <div className="container">
        <TaskList tasks={tasks} />
        {isCreateNew && <Task onTask={this.handleCreateNew} mapLastPosition={mapLastPosition} />}
        {isEdit && (
          <EditTask
            task={tasks[taskIndex]}
            onTask={this.handleSaveTask}
            mapLastPosition={mapLastPosition}
          />
        )}
        <GMap
          onMapClick={this.handleMapClick}
          markers={markers}
          mapLastPosition={mapLastPosition}
          onMarkerClick={this.handleMarkerClick}
        />
      </div>
    );
  }
}

export default DashboardView;
