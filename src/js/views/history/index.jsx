import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';

import { logActions } from '../../redux/modules/log';
import './history.css';

const mapStateToProps = ({ log }) => ({
  logEntries: log,
  isEmpty: log.length === 0,
});

const mapDispatchToProps = dispatch => ({
  log: bindActionCreators(logActions, dispatch),
});

const HistoryPage = ({ isEmpty, log, logEntries }) => (
  <div className="history__container">
    <h2>History</h2>
    {isEmpty || (
      <div className="reset" onClick={() => log.reset()} role="button" tabIndex="0">
        Reset
      </div>
    )}
    {isEmpty
      ? 'History is empty.'
      : logEntries.map(({ at, action: { id, type, description, location } }) => (
        <p key={`${at}`}>
          <em>
            {moment(new Date(at)).calendar()} ({type})
          </em>
          <strong>#{id}</strong>
          {description && description.replace(/<\/?strong>/gi, '')}
          {location && <span>Location: {location}</span>}
        </p>
      ))}
  </div>
);

export default connect(mapStateToProps, mapDispatchToProps)(HistoryPage);
