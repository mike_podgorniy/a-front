import React, { Component } from 'react';
import './page404.css';

const NotFoundPage = () => (
  <div className="page404">
    <h2>404</h2>
    <p>We Couldn't Find Your Page! (404 Error)</p>
  </div>
);

export default NotFoundPage;
